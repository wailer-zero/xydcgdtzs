<h1 align = "center">点右上角Start避免迷路，题库持续更新！</h1>
<h1 align = "center">《喜剧之王》联动答题题库已更新！</h1>

# 咸鱼大冲关答题助手

#### 介绍
```text
微信小游戏“咸鱼之王”咸鱼大冲关的答题助手
运行原理：
    助手的运行原理就是找到咸鱼之王的窗口后不断对窗口内区域进行截图、识别后返回对比结果
    ※注：其他界面识别出题目无妨，在答题界面内正常识别就行，毕竟目标是为了完成10关答题
作者：cnbreak：https://www.cnbreak.org
项目地址：https://gitee.com/cnbreak/xydcgdtzs
```

#### 贡献者
```text
馒头拿来摸摸：https://gitee.com/mtnlmm：解决了分辨率和缩放的问题
```

#### 文件介绍
```text
python-3.8.10-amd64.exe             win下python安装包
win_pip_install.bat                 win下依赖安装脚本
win_answerAssistant_v1.py           win下v1版程序
win_answerAssistant_v2.py           win下v2版程序
win_answerAssistant_v3.py           win下v3版程序
subject.py                          咸鱼之王题库
win_run.bat                         win下运行脚本
README.md                           项目描述文件
```

#### 运行平台
```text
目前仅windows系统，不限制具体版本号，只要python和微信能安装成功基本上都能用，理论上！
```

#### 特别注意
```text
1.代码是随意写的用于过关的娱乐代码，非商业化软件！不盈利！也严禁用于盈利！！！

2.不开程序都卡或无显低压CPU或A显低压CPU的电脑的用户们，听我一句劝，你们运行这个程序毫无意义，早点放弃对你好

3.程序默认安装的Paddle计算平台为CPU平台，CPU性能好的话1.5±0.5秒即可出结果，如果嫌CPU运行慢且你有英伟达显卡的话请自行安装CUDA平台

4.程序运行的过程中严禁"移动"或"遮挡"咸鱼之王的游戏窗体，否则会识别不准确，具体情况可看img文件夹内的图片和log日志

5.据使用过的小伙伴反馈说电脑"用户名"和"文件夹"不能是中文，否则运行会报错，也就是运行路径中不能包含中文！！！

6.有极小的概率识别出的内容与原题不符，应该是内容相似度的问题，极小概率，有人遇上过，我从没遇上过

7.遇到问题不要慌，按照使用教程的情况下不会出问题，出问题就再仔细阅读一遍"README.md"
```

#### 版本区别
```text
windows:
    win_*_v1：1920*1080分辨率和100%缩放运行
    win_*_v2：win_*_v1的基础上支持全分辨率和缩放
    win_*_v3：win_*_v2的基础上增加窗口显示状态判断
```

#### win下安装使用步骤
```text
1.右键单击"python-3.8.10-amd64.exe"，选择以管理员身份运行

2.选中底部"Add Python 3.8 to PATH"

3.选择"Customize installation"

4."Optional Features"界面右下角直接点击"Next"

5."Advanced Options"界面选中"Install for all users"和"Precompile standard library"

6."Advanced Options"界面点击"Install"

7.安装中...

8.看到"Setup was successful"后点击右下角"Close"

9.按"ctrl+r"键唤出"运行"输入"cmd"点"确定"打开控制台

10.控制台输"python -V"回车后显示“Python 3.8.10”代表python安装成功，不显示意味着漏了第2步，卸载后重装

11.双击"win_pip_install.bat"对程序运行所依赖的软件包进行安装，耗时视你当前网速快慢，耐心等待

12.pip安装完毕后请浏览整个安装过程是否出现过红色报错以确保依赖安装成功，红色为报错，黄色为警告

13.如果出现红色报错可以双击"win_pip_install.bat"再次重新安装下

14.运行助手前必须先在微信的小程序中打开咸鱼之王

15.开始答题之前双击"win_answerAssistant_v*.py"，首次运行会自动下载文字识别模型

16.待程序运行后开始识别题目时，点击"开始答题"即可
```

#### 报错解决
```python
错误：ValueError: Coordinate 'lower' is less than 'upper'
位置：win_answerAssistant_v2.py
错误原因：咸鱼之王的界面被最小化收进了任务栏中
解决方法：让咸鱼之王的窗体显示在桌面上就不会再报错了
是否修复：增加了窗口状态判断，已在win_answerAssistant_v3.py中修复
```

<h1 align = "center"> 娱乐代码！严禁用于盈利！！！</h1>
<h1 align = "center"> 娱乐代码！严禁用于盈利！！！</h1>
<h1 align = "center"> 娱乐代码！严禁用于盈利！！！</h1>

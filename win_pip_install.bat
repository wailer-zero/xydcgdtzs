python -m pip install --upgrade pip
pip install pypiwin32 -i https://mirrors.aliyun.com/pypi/simple
pip install fuzzywuzzy -i https://mirrors.aliyun.com/pypi/simple
pip install pillow -i https://mirrors.aliyun.com/pypi/simple
python -m pip install paddlepaddle==2.4.2 -i https://pypi.tuna.tsinghua.edu.cn/simple
pip install paddleocr -i https://mirrors.aliyun.com/pypi/simple
Pause